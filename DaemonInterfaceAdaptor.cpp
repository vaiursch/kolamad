/* 
 * kolamad - D-Bus daemon for easy acces to KoLaMa
 * 
 * For the latest version check: <http://gitorious.org/kolama/kolamad>
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamad ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamad. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "DaemonInterfaceAdaptor.hpp"

/*
 * Implementation of adaptor class DaemonInterfaceAdaptor
 */

DaemonInterfaceAdaptor::DaemonInterfaceAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    setAutoRelaySignals(true);
}

uint DaemonInterfaceAdaptor::frameheight() const
{
    // get the value of property frameheight
    return qvariant_cast< uint >(parent()->property("frameheight"));
}

void DaemonInterfaceAdaptor::setFrameheight(uint value)
{
    // set the value of property frameheight
    parent()->setProperty("frameheight", qVariantFromValue(value));
}

uint DaemonInterfaceAdaptor::framewidth() const
{
    // get the value of property framewidth
    return qvariant_cast< uint >(parent()->property("framewidth"));
}

void DaemonInterfaceAdaptor::setFramewidth(uint value)
{
    // set the value of property framewidth
    parent()->setProperty("framewidth", qVariantFromValue(value));
}

UniverseStatus DaemonInterfaceAdaptor::universes() const
{
    // get the value of property universes
    return qvariant_cast< UniverseStatus >(parent()->property("universes"));
}

uint DaemonInterfaceAdaptor::version() const
{
    return DAEMON_INTERFACE_VERSION; // HAND-EDIT
}

bool DaemonInterfaceAdaptor::setBox(int number, int x, int y, const QString &direction)
{
    // handle method call kosta.kolama.daemonInterface.setBox
    bool outofbounds;
    QMetaObject::invokeMethod(parent(), "setBox", Q_RETURN_ARG(bool, outofbounds), Q_ARG(int, number), Q_ARG(int, x), Q_ARG(int, y), Q_ARG(QString, direction));
    return outofbounds;
}

bool DaemonInterfaceAdaptor::setFrame(Frame frame)
{
    // handle method call kosta.kolama.daemonInterface.setFrame
    bool sizemismatch;
    QMetaObject::invokeMethod(parent(), "setFrame", Q_RETURN_ARG(bool, sizemismatch), Q_ARG(Frame, frame));
    return sizemismatch;
}


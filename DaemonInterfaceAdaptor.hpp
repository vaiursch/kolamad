/* 
 * kolamad - D-Bus daemon for easy acces to KoLaMa
 * 
 * For the latest version check: <http://gitorious.org/kolama/kolamad>
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamad ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamad. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef DAEMONINTERFACEADAPTOR_HPP
#define DAEMONINTERFACEADAPTOR_HPP

#include "AdditionalDBusTypes.hpp"

#include <QObject>
#include <QString>
#include <QtDBus>

/*
 * Adaptor class for interface kosta.kolama.daemonInterface
 */
class DaemonInterfaceAdaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "kosta.kolama.daemonInterface")

public:
    DaemonInterfaceAdaptor(QObject *parent);

    Q_PROPERTY(uint frameheight READ frameheight WRITE setFrameheight)
    uint frameheight() const;
    void setFrameheight(uint value);

    Q_PROPERTY(uint framewidth READ framewidth WRITE setFramewidth)
    uint framewidth() const;
    void setFramewidth(uint value);

    Q_PROPERTY(UniverseStatus universes READ universes)
    UniverseStatus universes() const;

    Q_PROPERTY(uint version READ version)
    uint version() const;

public slots:
    bool setBox(int number, int x, int y, const QString &direction);
    bool setFrame(Frame frame);
signals:
    void foundUniverse(int number);
    void lostUniverse(int number);

private:
    static const uint DAEMON_INTERFACE_VERSION = 1;
};

#endif


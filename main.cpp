/* 
 * kolamad - D-Bus daemon for easy acces to KoLaMa
 * 
 * For the latest version check: <http://gitorious.org/kolama/kolamad>
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamad ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamad. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "Daemon.hpp"
#include <QtCore>
#include <QtDBus>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    Daemon* daemon = new Daemon(&app);
    if(!QDBusConnection::sessionBus().registerObject("/daemon", daemon))
        return 1;
    if(!QDBusConnection::sessionBus().registerService("kosta.kolama"))
        return 1;
    
    return app.exec();
}

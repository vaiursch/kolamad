/* 
 * kolamad - D-Bus daemon for easy acces to KoLaMa
 * 
 * For the latest version check: <http://gitorious.org/kolama/kolamad>
 * 
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 * 
 * kolamad ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamad. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#ifndef DAEMON_HPP
#define DAEMON_HPP

#include <QObject>
#include <QString>
#include "AdditionalDBusTypes.hpp"

class DaemonInterfaceAdaptor;
class QDir;
class QFile;
class QFileSystemWatcher;

class Daemon : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint frameheight READ frameheight WRITE setFrameheight)
    Q_PROPERTY(uint framewidth READ framewidth WRITE setFramewidth)
    Q_PROPERTY(UniverseStatus universes READ universes)

    struct Point
    {
        int x;
        int y;
    };

    enum BoxDirection
    {
        Down,
        Right,
        Up,
        Left
    };

public:
    Daemon(QObject* parent);

    uint frameheight() const { return frameheight_; }
    void setFrameheight(uint value) { frameheight_ = value; }

    uint framewidth() const { return framewidth_; }
    void setFramewidth(uint value) { framewidth_ = value; }

    UniverseStatus universes() const { return universes_; }

public slots:
    bool setBox(int number, int x, int y, const QString& direction);
    bool setFrame(const Frame& frame);

signals:
    void foundUniverse(int number);
    void lostUniverse(int number);

private slots:
    void devchanged(const QString& path);

private:
    BoxDirection directionFromString(const QString& direction);
    Frame makeTestFrame();
    void changedUniverses();

    Point box_[8];
    BoxDirection box_direction_[8];
    uint frameheight_;
    uint framewidth_;
    UniverseStatus universes_;

    DaemonInterfaceAdaptor* dadaptor_;
    QDir* devdir_;
    QFile* tty1_;
    QFile* tty2_;
    QFileSystemWatcher* devwatcher_;
};

#endif

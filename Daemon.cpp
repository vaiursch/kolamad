/*
 * kolamad - D-Bus daemon for easy acces to KoLaMa
 *
 * For the latest version check: <http://gitorious.org/kolama/kolamad>
 *
 * Copyright 2010 Stiftung KOSTA <http://www.kosta.ch>
 *
 * kolamad ist free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kolamad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kolamad. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 *      Erwin 'vaiursch' Herrsche
 *
 */

#include "Daemon.hpp"
#include "DaemonInterfaceAdaptor.hpp"

#include <QtCore>

Daemon::Daemon(QObject* parent)
	: QObject(parent)
{
	registerAdditionalTypes();
	frameheight_ = 8;
	framewidth_ = 16;
	for(int i = 1; i <= 8; i++) {
		setBox(i, 15, 0, "down");
	}

	dadaptor_ = new DaemonInterfaceAdaptor(this);
	devdir_ = new QDir("/dev", "ttyUSB*");
	QStringList devlist = devdir_->entryList(QDir::System, QDir::Name);
	qDebug() << devlist;

	universes_.universe1 = false;
	universes_.universe2 = false;

	switch(devlist.size()) {
		case 0:
			tty1_ = new QFile("", this);
			tty2_ = new QFile("", this);
			break;

		case 1:
			tty1_ = new QFile("/dev/" + devlist[0], this);
			universes_.universe1 = true;
			tty2_ = new QFile("", this);
			break;

		default:
			tty1_ = new QFile("/dev/" + devlist[0], this);
			universes_.universe1 = true;
			tty2_ = new QFile("/dev/" + devlist[1], this);
			universes_.universe2 = true;
			break;
	}
	changedUniverses();

	devwatcher_ = new QFileSystemWatcher(this);
	devwatcher_->addPath("/dev");
	connect(devwatcher_, SIGNAL(directoryChanged(const QString&)), this, SLOT(devchanged(const QString&)));

	setFrame(makeTestFrame());
}

bool Daemon::setBox(int number, int x, int y, const QString& direction)
{
	if(number < 1 || number > 8) {
		qDebug() << number << " is not a valid box number.";
		return false;
	}

	box_[number-1].x = x;
	box_[number-1].y = y;
	box_direction_[number-1] = directionFromString(direction);

	qDebug() << "Box " << number << " set to x:" << x << " y:" << y << " dir:" << directionFromString(direction);
	return true;
}

bool Daemon::setFrame(const Frame& frame)
{
	bool ret = true;
	if((uint)frame.size() != framewidth())
		ret = false;
	QByteArray column;
	foreach(column, frame) {
		if((uint)column.size() != frameheight())
			ret = false;
	}

	QByteArray msg1(518, 0x00);
	msg1[0] = 0x7E; // start delimiter
	msg1[1] = 0x06; // send DMX label
	msg1[2] = 0x01; // datasize is 513, all channels + starcode (0x00)
	msg1[3] = 0x02;
	msg1[517] = 0xE7; // end delimiter

	QByteArray msg2(518, 0x00);
	msg2[0] = 0x7E; // start delimiter
	msg2[1] = 0x06; // send DMX label
	msg2[2] = 0x01; // datasize is 513, all channels + starcode (0x00)
	msg2[3] = 0x02;
	msg2[517] = 0xE7; // end delimiter

	for(int i = 0; i < 8; i++) {
		int x0, y0, x, y;
		switch(box_direction_[i]) {
			case Down:
				x0 = box_[i].x;
				y0 = box_[i].y;
				for(int dx = 0; dx < 16; dx++) {
					for(int dy = 0; dy < 8; dy++) {
						x = x0-dx;
						y = y0+dy;
						if(x < (int)framewidth() && y < (int)frameheight() && x < frame.size() && y < frame[x].size()) {
							if(i < 4)
								msg1[i*128+dx*8+dy+5] = frame[x][y];
							else
								msg2[(i-4)*128+dx*8+dy+5] = frame[x][y];
						}
					}
				}
			break;

			case Up:
				x0 = box_[i].x;
				y0 = box_[i].y;
				for(int dx = 0; dx < 16; dx++) {
					for(int dy = 0; dy < 8; dy++) {
						x = x0+dx;
						y = y0-dy;
						if(x < (int)framewidth() && y < (int)frameheight() && x < frame.size() && y < frame[x].size()) {
							if(i < 4)
								msg1[i*128+dx*8+dy+5] = frame[x][y];
							else
								msg2[(i-4)*128+dx*8+dy+5] = frame[x][y];
						}
					}
				}
			break;

			case Right:
				x0 = box_[i].x;
				y0 = box_[i].y;
				for(int dy = 0; dy < 16; dy++) {
					for(int dx = 0; dx < 8; dx++) {
						x = x0+dx;
						y = y0+dy;
						if(x < (int)framewidth() && y < (int)frameheight() && x < frame.size() && y < frame[x].size()) {
							if(i < 4)
								msg1[i*128+dy*8+dx+5] = frame[x][y];
							else
								msg2[(i-4)*128+dy*8+dx+5] = frame[x][y];
						}
					}
				}
			break;

			case Left:
				x0 = box_[i].x;
				y0 = box_[i].y;
				for(int dy = 0; dy < 16; dy++) {
					for(int dx = 0; dx < 8; dx++) {
						x = x0-dx;
						y = y0-dy;
						if(x < (int)framewidth() && y < (int)frameheight() && x < frame.size() && y < frame[x].size()) {
							if(i < 4)
								msg1[i*128+dy*8+dx+5] = frame[x][y];
							else
								msg2[(i-4)*128+dy*8+dx+5] = frame[x][y];
						}
					}
				}
			break;
		}
	}

	if(tty1_->fileName() != "") {
		if(!tty1_->open(QIODevice::WriteOnly | QIODevice::Text))
			qDebug() << "Could not open first universe: " << tty1_->errorString();
		if(tty1_->write(msg1) == -1) qDebug() << "Write on first universe failed: " << tty1_->errorString();
		tty1_->close();
	}

	if(tty2_->fileName() != "") {
		if(!tty2_->open(QIODevice::WriteOnly | QIODevice::Text))
			qDebug() << "Could not open first universe: " << tty2_->errorString();
		if(tty2_->write(msg2) == -1) qDebug() << "Write on second universe failed: " << tty2_->errorString();
		tty2_->close();
	}

	return ret;
}

Daemon::BoxDirection Daemon::directionFromString(const QString& direction)
{
	if(direction.toLower() == "down")
		return Down;
	if(direction.toLower() == "right")
		return Right;
	if(direction.toLower() == "up")
		return Up;
	if(direction.toLower() == "left")
		return Left;

	return Down;
}

void Daemon::devchanged(const QString& path) {
	if(path == "/dev") {
		QStringList devlist = devdir_->entryList(QDir::System, QDir::Name);
		qDebug() << devlist;

		if(!devlist.contains(tty1_->fileName())) {
			universes_.universe1 = false;
			tty1_->setFileName("");
			changedUniverses();
		}

		if(!devlist.contains(tty2_->fileName())) {
			universes_.universe2 = false;
			tty2_->setFileName("");
			changedUniverses();
		}

		devlist.removeAll(tty1_->fileName());
		devlist.removeAll(tty2_->fileName());

		if(!devlist.isEmpty()) {
			if(tty1_->fileName() == "") {
				universes_.universe1 = true;
				tty1_->setFileName(devlist[0]);
				changedUniverses();
			}
			devlist.removeAll(tty1_->fileName());
			if(!devlist.isEmpty()) {
				if(tty2_->fileName() == "") {
					universes_.universe2 = true;
					tty2_->setFileName(devlist[0]);
					changedUniverses();
				}
			}
		}
	}

	return;
}

Frame Daemon::makeTestFrame()
{
	const char frame[][8] = {{0,0,0, 0xFF,0xFF,0xFF,0xFF,0xFF},
					   {0,0,0,0,0, 0xFF},
					   {0,0,0,0, 0xFF, 0, 0xFF},
					   {0,0,0, 0xFF, 0,0,0, 0xFF},
					   {0xFF,0xFF,0xFF},
					   {0xFF, 0, 0xFF},
					   {0xFF,0xFF,0xFF},
					   {0,0,0, 0xFF,0xFF,0xFF, 0 ,0xFF},
					   {0,0,0, 0xFF, 0 ,0xFF, 0 ,0xFF},
					   {0,0,0, 0xFF, 0 ,0xFF,0xFF,0xFF},
					   {0xFF},
					   {0xFF,0xFF,0xFF},
					   {0xFF},
					   {0,0,0, 0xFF,0xFF,0xFF,0xFF,0xFF},
					   {0,0,0, 0xFF, 0 ,0xFF},
					   {0,0,0, 0xFF,0xFF,0xFF,0xFF,0xFF}};

	Frame ret;
	for(int i = 0; i < 16; i++) {
		QByteArray array(frame[i],8);
		ret.append(array);
	}
	return ret;
}

void Daemon::changedUniverses()
{
	if(universes_.universe1)
		emit foundUniverse(1);
	else
		emit lostUniverse(1);

	if(universes_.universe2)
		emit foundUniverse(2);
	else
		emit lostUniverse(2);
}

